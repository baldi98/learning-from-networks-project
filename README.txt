The Networks folder contains the CSV files of each network.
The main code is in LFN_Project.py, inside it we commented all lines regarding the printing of measures and scores. If needed you can uncomment the ones you want to see.
The file containing all functions used in the main code is utils.py.
