import os
import glob
import numpy as np
import networkx as nx
import pandas as pd
from numpy import genfromtxt
import matplotlib.pyplot as plt
import scipy.special as sc
import scipy
from scipy import stats
import pydot
import graphviz
from scipy.interpolate import interp1d
from utils import truncateNumber, information, secrecy, destabilization

# LOADING AND PLOTTING GRAPHS 

# Path to Networks folder
input_path = "Networks"

# List that will contain graphs
graph_list = []

# Reading and inserting each graph in a list
for element in sorted(glob.glob("Networks/*")): 

    # H will be the final graph
    H = nx.Graph()

    # Obtaining paths to CSV files
    folder = os.listdir(element)
    local_path = os.path.join(element, folder[0]).replace("\\", "/")

    # Creating the final graph
    for filename in os.listdir(local_path):

        # Reading a single CSV file and storing it in a numpy array
        mydata = genfromtxt(os.path.join(local_path, filename).replace("\\", "/"), delimiter=',')

        # Selecting the names of the nodes
        names = mydata[0:1, 0:]
        names = np.delete(names, 0)

        # Extracting the adjacency matrix from numpy array
        adjacency = mydata[1:, 1:]

        # Dictionary used to change the default nodes' names with the correct ones
        nodes_names = {i: int(names[i]) for i in range(0, len(names))}

        # Obtaining the graph from the adjacency matrix
        G = nx.convert_matrix.from_numpy_array(adjacency)

        # Changing nodes' names
        G = nx.relabel_nodes(G, mapping=nodes_names)

        # Composition of the different graphs in a unique one
        H = nx.compose(G, H)

    # Inserting each graph in a list
    graph_list.append(H)
    
titles = ["Bali B.", "Hamburg C.", "Jakarta B.", "Madrid B."]

# Plotting graphs
for i, H in enumerate(graph_list):

    # Removing isolated nodes in Hamburg Cell
    if i == 1:
        H.remove_node(1040)
        H.remove_node(54)
        H.remove_node(1036)

    # Creating a figure to plot the graph
    plt.figure(str(i), figsize=(12, 7))
    plt.title(titles[i])

    # Choosing nodes' color
    color_map = ['yellow' for i in range(0, H.number_of_nodes())]

    # Plotting of the final graph
    nx.draw(H, with_labels=True, node_size=500, font_size=11, node_color=color_map)
    plt.draw()
plt.show()

# COMPUTING SNA MEASURES

# Initialization of lists of measures
tot_edges, tot_nodes, tot_connectivity, tot_density, tot_nodal_degree = [], [], [], [], []
tot_degree, tot_closeness, tot_betweenness, tot_cc, tot_global_cc = [], [], [], [], []

# Computing some measures
for H in graph_list:

    # 1 - Size
    edges = H.size()
    tot_edges.append(edges)
    nodes = H.number_of_nodes()
    tot_nodes.append(nodes)
    
    # 2 - Connectivity
    number_components = nx.number_connected_components(H)
    tot_connectivity.append(number_components)

    # 3 - Density
    density = nx.density(H)
    tot_density.append(truncateNumber(density))
    
    # 4 - Nodal Degree
    nodal_degrees = H.degree()
    nodal_degrees = sorted(list(nodal_degrees), key=lambda tup: tup[1], reverse=True)
    tot_nodal_degree.append(nodal_degrees)
            
    # 5 - Node Degree Centrality
    degree_centralities = nx.degree_centrality(H)
    tot_degree.append(truncateNumber(degree_centralities))

    # 6 - Node Closeness Centrality
    closeness = nx.closeness_centrality(H)
    tot_closeness.append(truncateNumber(closeness))

    # 7 - Node Betweenness Centrality
    betweenness = nx.betweenness_centrality(H)
    tot_betweenness.append(truncateNumber(betweenness))

    # 8 - Local Clustering Coefficient
    cc = nx.clustering(H)
    tot_cc.append(truncateNumber(cc))

    # 9 - Global Clustering Coefficient
    triangles = sum(nx.triangles(H).values())/3
    global_cc = triangles/(sc.binom(nodes, 3))
    tot_global_cc.append(global_cc)

# print("Number of nodes: ", tot_nodes)
# print("Number of edges: ", tot_edges)
# print("Connected Components: ", tot_connectivity)
# print("Densities: ", tot_density)
# print("Nodal Degree: ", tot_nodal_degree)
# print("Degree Centrality: ", tot_degree)
# print("Closeness Centrality: ", tot_closeness)
# print("Betweenness Centrality: ", tot_betweenness)
# print("Local Clustering Coefficients: ", tot_cc)
# print("Global C.C.: ", tot_global_cc)

# INFORMATION AND SECRECY

# Initialization of the list containing Information measures 
tot_information = []

# Computing Information measure for each graph
for graph in graph_list:
    tot_information.append(information(graph))

# print("Informations: ", tot_information)

# Initialization of the list containing Secrecy measures
tot_secrecy = []

# Computing Secrecy measure for each graph
for graph in graph_list:
    tot_secrecy.append(secrecy(graph))
# print("Secrecies: ", tot_secrecy)

# DESTABILIZATION

# Initialization of the list containing the trees for each component
all_trees = []

# Computing destabilization for each graph
for graph in graph_list:
    all_trees.append(destabilization(graph))

# Plotting trees for each graph
counter = 0
titles=["Bali B.", "Hamburg C.", "Jakarta B. 1", "Jakarta B. 2", "Jakarta B. 3", "Madrid B."]
for tree_list in all_trees:
    for tree in tree_list:
        counter += 1
        plt.figure(str(counter), figsize=(12, 7))
        plt.title(titles[counter-1])
        color_map = ['yellow' for i in range(0, tree.number_of_nodes())]
        pos = nx.nx_pydot.graphviz_layout(tree, prog='dot')
        nx.draw(tree, pos, with_labels=True, node_size=500, font_size=11, node_color=color_map)
plt.show()

# SIGNIFICANCE

# Initialization of lists for z-scores
total_zscore_cc, total_zscore_secrecy, total_zscore_information = [], [], []


for e,graph in enumerate(graph_list):

    # Degree sequence
    degree_sequence = [d for n, d in graph.degree()]

    # Initialization of lists for Global Clustering Coefficients, Secrecies and Information of random graphs
    random_global_cc, random_secrecy, random_information = [], [], []

    # 500 random graphs
    for i in range(500):

        # Generating a random graph
        random_graph = nx.expected_degree_graph(degree_sequence)

        # Global Clustering Coefficient of the random graph
        triangles = sum(nx.triangles(random_graph).values())/3
        global_cc = triangles/(sc.binom(random_graph.number_of_nodes(), 3))
        random_global_cc.append(global_cc)

        # Secrecy measure of the random graph
        secrecy_random_graph = secrecy(random_graph)
        random_secrecy.append(secrecy_random_graph)

        # Information measure of the random graph
        information_random_graph = information(random_graph)
        random_information.append(information_random_graph)

    # Mean of Global Clustering Coefficients, Secrecies and Information for the random graphs
    mean_cc = np.mean(random_global_cc)
    mean_secrecy = np.mean(random_secrecy)
    mean_info = np.mean(random_information)

    # Standard deviation of Global Clustering Coefficients, Secrecies and Information for the random graphs
    std_cc = np.std(random_global_cc)
    std_secrecy = np.std(random_secrecy)
    std_info = np.std(random_information)

    # Z-scores for Global Clustering Coefficient, Secrecy and Information
    total_zscore_cc.append((tot_global_cc[e] - mean_cc)/std_cc)
    total_zscore_secrecy.append((tot_secrecy[e] - mean_secrecy)/std_secrecy)
    total_zscore_information.append((tot_information[e] - mean_info)/std_info)

# print("Z-score Clustering Coefficient: ", total_zscore_cc)
# print("Z-score Secrecy: ", total_zscore_secrecy)
# print("Z-score Information: ", total_zscore_information)

# COMPUTING AND PLOTTING DEGREES DISTRIBUTION

final_plot = []

for e, graph in enumerate(graph_list):

    # Selecting degree values of graph e
    degrees_for_plot = tot_nodal_degree[e]
    degree_values = []
    for pair in degrees_for_plot:
        degree_values.append(pair[1])

    # Dictionary containing degree-frequency pairs
    to_plot = {}

    # For every degree value the frequency is computed
    for value in degree_values:
        counter = 0
        for pair in degrees_for_plot:
            if pair[1] == value:
                counter += 1
        to_plot[value] = counter
    
    # Containing the dictionary of every graph
    final_plot.append(to_plot)

# Constructing the image of degrees distribution
plt.figure(figsize=(8, 5))

f1 = interp1d(list(final_plot[0].keys()), list(final_plot[0].values()))
f2 = interp1d(list(final_plot[1].keys()), list(final_plot[1].values()))
f3 = interp1d(list(final_plot[2].keys()), list(final_plot[2].values()))
f4 = interp1d(list(final_plot[3].keys()), list(final_plot[3].values()))


xnew1 = np.linspace(2, 25, num=27, endpoint=True)
xnew2 = np.linspace(1, 20, num=61, endpoint=True)
xnew3 = np.linspace(1, 10, num=61, endpoint=True)
xnew4 = np.linspace(1, 25, num=61, endpoint=True)


plt.plot(xnew1, f1(xnew1), '-',  label="Bali Bombing")
plt.plot(xnew2, f2(xnew2), '-',  label="Hamburg Cell")
plt.plot(xnew3, f3(xnew3), '-',  label="Jakarta Bombing")
plt.plot(xnew4, f4(xnew4), '-',  label="Madrid Bombing")

plt.xticks(list(range(26)), list(range(26)))
plt.legend(loc="upper right")
plt.show()


