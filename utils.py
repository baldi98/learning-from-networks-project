import networkx as nx


# Round each item in a list/dict of floats to 2 decimal places
def truncateNumber(list_of_numbers):
    
    # Dict case
    if isinstance(list_of_numbers, dict):
        for element in list_of_numbers.keys():
            list_of_numbers[element] = float("%.2f" % list_of_numbers[element])
    
    # List case
    elif isinstance(list_of_numbers, list):
        for i, element in enumerate(list_of_numbers):
            list_of_numbers[i] = float("%.2f" % list_of_numbers[i])
     
    # Other cases        
    else:
        list_of_numbers = float("%.2f" % list_of_numbers)
    
    return list_of_numbers


# Computing Information measure for a graph
def information(graph):

    # Storing in a list the components of a graph
    components = [graph.subgraph(c).copy() for c in nx.connected_components(graph)]
    
    # Initialization of component's Information measure
    component_information = 0

    # Computing Information measure for each component
    for component in components:
        # Computing the number of nodes for each component
        component_nodes = component.number_of_nodes()

        if component_nodes == 1:
            break

        # Computing the shortest path length between all nodes in the component
        shortest_paths = dict(nx.all_pairs_shortest_path_length(component))
        # Summing the lengths to individual node
        sum_shortest_path = {node1: sum([length for length in shortest_paths[node1].values()]) for node1 in shortest_paths.keys()}

        # Initialization of the total distance
        total_distance = 0

        # Summing the lengths of all paths
        for node,length in sum_shortest_path.items():
            total_distance += length

        # Computing and summing the information of each component
        component_information += (component_nodes*(component_nodes-1))/(total_distance)

    # Graph's Information measure
    information = component_information/len(components)

    return information


# Computing Secrecy measure for a graph
def secrecy(graph):

    # Number of nodes of the graph
    nodes = graph.number_of_nodes()
    # Size of the graph
    edges = graph.size()
    # List containing the degree of each node in the graph
    degrees = list(graph.degree())

    # Initialization of the variable containing the sum of the degrees squared
    tot_degree = 0

    # Computing Secrecy measure for the graph
    for degree in degrees:
        tot_degree += pow(degree[1], 2)
    secrecy = (2*edges*(nodes-2)+nodes*(nodes-1)-tot_degree)/((2*edges+nodes)*nodes)

    return secrecy


def destabilization(graph):

    # List containing the tree for each component
    tree_components = []

    # Components of the graph
    components = [graph.subgraph(c).copy() for c in nx.connected_components(graph)]

    for component in components:

        # PageRank Centrality
        pr = nx.pagerank(component)
        # Katz Centrality
        kc = nx.katz_centrality_numpy(component)

        # Directed tree with a "root" node
        tree = nx.DiGraph()
        tree.add_node("root")

        for node in list(component):

            # List of neighbors of a node
            neighbors = [n for n in component.neighbors(node)]

            # Initialization of parent and children lists to build the tree
            parent_list = []
            children_list = []

            for neighbor in neighbors:

                # Using PageRank Centrality

                # If the PageRank Centrality of the neighbor is greater than the 
                # PageRank of the node, add the neighbor to the parent list
                if pr[neighbor] > pr[node]:
                    parent_list.append(neighbor)

                # If the PageRank Centrality of the neighbor is smaller than the 
                # PageRank of the node, add the neighbor to the children list
                elif pr[neighbor] < pr[node]:
                    children_list.append(neighbor)

                # Using Katz Centrality
                else:

                    # If the Katz Centrality of the neighbor is greater than the 
                    # Katz Centrality of the node, add the neighbor to the parent list
                    if kc[neighbor] > kc[node]:
                        parent_list.append(neighbor)

                    # If the Katz Centrality of the neighbor is smaller than the 
                    # Katz Centrality of the node, add the neighbor to the children list
                    elif kc[neighbor] < kc[node]:
                        children_list.append(neighbor)

            # If parent_list is empty, add the considered node to the tree as a children of "root"
            if len(parent_list) == 0:
                tree.add_node(node)
                tree.add_edge("root", node)

            # If parent_list contains 1 node, add it to the tree as the parent of the considered node
            elif len(parent_list) == 1:
                tree.add_node(parent_list[0])
                tree.add_node(node)
                tree.add_edge(parent_list[0], node)

            # If parent_list contains more than 1 node, consider the maximum intersection between
            # the neighbors of nodes in parent_list and the neighbors of the considered node
            elif len(parent_list) > 1:

                max_intersection = 0
                selected_node = None

                for parent in parent_list:
                    # Set of neighbors of a parent in parent_list
                    parent_neighbors = [n for n in component.neighbors(parent)]
                    # Size of the intersection between the neighbors of 
                    # a parent node and the neighbors of the considered node
                    size_intersection = len(list(set(parent_neighbors).intersection(neighbors)))

                    # If the size of the intersection is 0, ignore the node
                    if size_intersection == 0:
                        break

                    if size_intersection > max_intersection:
                        # Maximum intersection
                        max_intersection = size_intersection
                        # Parent for which the intersection is maximum
                        selected_node = parent
                
                # Add the selected parent to the tree as the parent of the considered node
                if selected_node is not None:
                    tree.add_node(selected_node)
                    tree.add_node(node)
                    tree.add_edge(selected_node, node)

                else:
                    tree.add_node(node)
                    tree.add_edge("root", node)
         
        # Add the tree of the component to the list of trees
        tree_components.append(tree)

    return tree_components

